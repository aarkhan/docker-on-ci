# Docker on CI

[![Github Action](https://github.com/aarkhan/docker-on-ci/workflows/build/badge.svg)](https://github.com/aarkhan/docker-on-ci/actions)
[![GitlabCI](https://gitlab.com/aarkhan/docker-on-ci/badges/master/pipeline.svg)](https://gitlab.com/aarkhan/docker-on-ci/pipelines)
[![CircleCI](https://circleci.com/gh/aarkhan/docker-on-ci.svg?style=svg)](https://circleci.com/gh/aarkhan/docker-on-ci)
[![travis](https://api.travis-ci.org/aarkhan/docker-on-ci.svg?branch=master)](https://travis-ci.org/aarkhan/docker-on-ci/builds)